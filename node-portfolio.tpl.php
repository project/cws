<?php
?>
<?php phptemplate_comment_wrapper(NULL, $node->type); ?>




<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
<?php if ($sticky)
		print '
		<div class="stick_n">
		<div class="stick_e">
		<div class="stick_s">
		<div class="stick_w">
		<div class="stick_ne">
		<div class="stick_se">
		<div class="stick_sw">
		<div class="stick_nw">';
	else
		print '
		<div class="port_n">
		<div class="port_e">
		<div class="port_s">
		<div class="port_w">
		<div class="port_ne">
		<div class="port_se">
		<div class="port_sw">
		<div class="port_nw">';
	?>

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print t('!date — !username', array('!username' => theme('username', $node), '!date' => format_date($node->created))); ?></span>
  <?php endif; ?>

  <div class="content">
    <?php print $content ?>
  </div>

    <div class="meta">
    <?php if ($taxonomy): ?>
    	<hr /><br />
      <div class="terms"><strong>Tags - </strong><?php print $terms ?></div>
    	<br />
    <?php endif;?>
    

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
    </div>

</div>
<?php if ($sticky)
    print '</div></div></div></div></div></div></div></div>';
  else
    print '</div></div></div></div></div></div></div></div>';
  ?>

<br />