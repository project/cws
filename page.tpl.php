<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
    <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/fix-ie.css";</style>
    <![endif]-->
  </head>
  <body<?php print phptemplate_body_class($sidebar_left, $sidebar_right); ?>>

<!-- Layout -->
    <div id="wrapper">
    <div id="topSkip">
	<ul>
		<li><a href="#content">Skip to content</a></li>
		<li><a href="#sideNav">Skip to side navigation bar</a></li>
		<li><a href="/accessibility-statement">Accessibility Statement</a></li>
	</ul>	
	</div>
      <div id="header">
        <?php print $header; ?>
        <?php
          // Prepare header
          $site_fields = array();
          if ($site_name) {
            $site_fields[] = check_plain($site_name);
          }
          if ($site_slogan) {
            $site_fields[] = check_plain($site_slogan);
          }
          $site_title = implode(' ', $site_fields);
          $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          $site_html = implode(' ', $site_fields);

          if ($logo || $site_title) {
            print '<h1><a href="'. check_url($base_path) .'" title="'. $site_title .'">';
            if ($logo) {
              print '<img class="logo" src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
             print $site_html .'</a></h1>';
          }
        ?>
        </div>

      <div id="nav">
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>
        <?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        <?php endif; ?>
      </div>
      <div id="container">
      <div class="container_n">
		<div class="container_e">
		<div class="container_s">
		<div class="container_w">
		<div class="container_ne">
		<div class="container_se">
		<div class="container_sw">
		<div class="container_nw">
        <?php if ($sidebar_left): ?>
          <div id="sidebar-left" class="sidebar">
            <?php if ($search_box): ?><?php      
            print str_replace("<div id=\"search\" class=\"container-inline\"><div class=\"form-item\">", "<div id=\"search\" class=\"container-inline\"><div class=\"form-item\"><label for=\"edit-search-theme-form-keys\"></label>", $search_box)
            ?><?php endif; ?>
            <a name="sideNav" id="sideNav"></a>
            <?php print $sidebar_left ?>
          </div>
        <?php endif; ?>
			
			
			
        <div id="center">
      	  		<div class="center_n">
				<div class="center_e">
				<div class="center_s">
				<div class="center_w">
				<div class="center_ne">
				<div class="center_se">
				<div class="center_sw">
				<div class="center_nw">
        
        
        <a name="content" id="content"></a>
        	<?php if ($content_top): print $content_top; endif; ?>
        	
        	<?php if ($breadcrumb): print $breadcrumb; endif; ?>
			<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
			<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
			<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
			<?php if ($tabs): print $tabs .'</div>'; endif; ?>
			<?php if (isset($tabs2)): print $tabs2; endif; ?>
			<?php if ($help): print $help; endif; ?>
			<?php if ($messages): print $messages; endif; ?>

			<?php print $content ?>
			<div id="footer"><?php print $feed_icons ?></div>
          
        </div> <!-- /#center -->
        </div></div></div></div></div></div></div></div> <!-- Container Border -->

        <?php if ($sidebar_right): ?>
          <div id="sidebar-right" class="sidebar">
            <?php if (!$sidebar_left && $search_box): ?><?php      
            print str_replace("<div id=\"search\" class=\"container-inline\"><div class=\"form-item\">", "<div id=\"search\" class=\"container-inline\"><div class=\"form-item\"><LABEL for=\"edit-search-theme-form-keys\"></LABEL>", $search_box)
            ?><?php endif; ?>
            <?php print $sidebar_right ?>
          </div>
        <?php endif; ?>

        <span class="clear"></span>
        <div id="footer"><?php print $footer_message ?></div>
      </div> <!-- /container -->
      <span class="clear"></span>
      
      </div></div></div></div></div></div></div></div>
    </div>
<!-- /layout -->


  <?php print $closure ?>
  </body>
</html>