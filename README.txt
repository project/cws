
Drupal 5.7 Christian Web Solutions theme
Created by Paul Merrick > fourtyfreak
Derived from Framework theme by Andre Griffin

CWS was designed to try and fill the requirement for an accessible Drupal theme. Framework was chosen as the backbone for this theme, but a number of modifications have been made to satisfy WCAG requrements.

	* CWS retains the fixed width layout with autoresizing center column if either left or right sidebars are disabled.
	* All fonts are stated in relative sizes.
	* Colour contrast of the elements pass automated testing.
	* Administration section retains the Garland derived CSS.
	
	
Exeptions to WCAG 1.0

	* Pre-filled form fields
		* Checkpoint 10.4 of WCAG 1.0 recommends that until user agents (such as browsers or screen readers) are capable of recognising text fields that do not contain default text, text fields in forms should be pre-filled with text.
		
		* User testing, combined with evidence from our web stats, show that this is no longer a necessity for accessibility. Research has in fact shown that pre-filled text fields can cause difficulties for some modern screen readers, therefore we do not include them. Prefilling form fields is due to be dropped as a requirement from WCAG 2.0.


	* Accesskeys
		* Checkpoint 9.5 recommends providing keyboard shortcuts to important links, often called "accesskeys". However, these have also caused problems, and our own and other people's testing have demonstrated that they are best left off public websites. The accesskey requirement is due to be dropped from WCAG 2.0.


Installation

	Upload cws folder via FTP to sites/all/themes
	
	Enable theme in admin/build/themes menu.
	
	