<?php

/**
 * Declare regions
 */

function cws_regions() {
  return array(
    'sidebar_right' => t('right sidebar'),
    'sidebar_left' => t('left sidebar'),
    'content_top' => t('content top'),
    'content' => t('content'),
    'header' => t('header'),
    'footer' => t('footer'),
  );
}

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($sidebar_left, $sidebar_right) {
  if ($sidebar_left != '' && $sidebar_right != '') {
    $class = 'sidebars';
  }
  else {
    if ($sidebar_left != '') {
      $class = 'sidebar-left';
    }
    if ($sidebar_right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function _phptemplate_variables($hook, $vars) {

   if (module_exists('advanced_forum')) {
 	   $vars = advanced_forum_addvars($hook, $vars);
 	  }
 	  
 	  return $vars;


  if ($hook == 'page') {

    if ($secondary = menu_secondary_local_tasks()) {
      $output = '<span class="clear"></span>';
      $output .= "<ul class=\"tabs secondary\">\n". $secondary ."</ul>\n";
      $vars['tabs2'] = $output;
    }

    // Hook into color.module
    if (module_exists('color')) {
      _color_page_alter($vars);
    }
    return $vars;
  }
  return array();

}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">\n". $primary ."</ul>\n";
  }

  return $output;
}

function phptemplate_tinymce_theme($init, $textarea_name, $theme_name, $is_running) {
  switch($textarea_name){
    case 'nodewords-description':
    case 'edit-nodewords-description':
        case 'title': //title field - necessary for faq_ask questions. 
        case 'log': // book and page log
        unset($init);
        return $init;
        break;
    default:
        return theme_tinymce_theme($init, $textarea_name, $theme_name, $is_running);
  }
}

/**
 * Forum unread post view
 */

function phptemplate_views_view_table_forumTracker($view, $nodes, $type) {
  $fields = _views_get_fields();
  $comments_per_page = _comment_get_display_setting('comments_per_page');

  foreach ($nodes as $node) {
    $row = array();
    foreach ($view->field as $field) {
      if ($fields[$field['id']]['visible'] !== FALSE) {
if($field['field'] == 'comment_count')
{
      $count = db_result(db_query('SELECT COUNT(c.cid) FROM {comments} c WHERE c.nid=%d', $node->nid));
    // Find the ending point. The pager URL is always 1 less than
    // the number being displayed because the first page is 0.
            $last_display_page = ceil($count / $comments_per_page);
            $last_pager_page = $last_display_page - 1;

    $cell['data'] = $node->node_comment_statistics_comment_count.'<br>'.l('new', 'node/'.$node->nid, array(), 'page='.$last_pager_page.'#new');
            $cell['class'] = "view-field ". views_css_safe('view-field-'. $field['queryname']);
            $row[] = $cell;
}
else
{
$cell['data'] = views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
                    $cell['class'] = "view-field ". views_css_safe('view-field-'. $field['queryname']);
                $row[] = $cell;
}
      }
    }
    $rows[] = $row;
  }
  return theme('table', $view->table_header, $rows);
}
